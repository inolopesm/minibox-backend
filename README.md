# Minibox (backend)

Repositório referente a uma idéia gerada a partir da necessidade que se há no gerenciamento de devedores a equipe minibox (minimercado, mercado, e entre outros nomes), da qual faz parte de um EJC (Encontro de Jovens com Cristo), advindo do movimento jovem da Igreja Católica.

## Requisitos funcionais

- [x] Gerenciar todas as equipes.
- [x] Gerenciar todos os participantes, identificando-os a sua equipe, a partir do momento da abertura de sua conta para inserção de um pedido.
- [x] Gerenciar todos os pedidos dos participantes cadastrados, tendo a identificação se o pedido foi pago ou não.
- [x] Gerenciar todos os produtos disponíveis.
- [x] Gerenciar todos os produtos dentro dos pedidos.
- [ ] Implementar uma camada de autenticação, tendo grupos de autorização limitando sua atuação na aplicação

## Requisitos não-funcionais
- [x] Entrada/Saída em formado API REST

## Ferramentas
- [Node.js](https://nodejs.org/pt-br/)
- [Typescript](https://www.typescriptlang.org/)
- [Express](http://expressjs.com/pt-br/)
- [Sequelize ORM](https://sequelize.org/)
- [Yup](https://github.com/jquense/yup)
- [Nodemon](https://nodemon.io/)
- [Sucrase](https://github.com/alangpierce/sucrase)
- [ESLint](https://eslint.org/)
- [dotenv-safe](https://github.com/rolodato/dotenv-safe)
- [env-var](https://github.com/evanshortiss/env-var)
- [morgan](https://github.com/expressjs/morgan)
- [Helmet](https://helmetjs.github.io/)
- [cors](https://github.com/expressjs/cors)

## Diagrama
![Diagrama](https://gitlab.com/matheuslopess1/minibox-backend/-/raw/master/docs/diagram.png)