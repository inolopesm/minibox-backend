import { Request, Response } from "express";
import { Customer } from "../models";

class CustomerController {
  async index(req: Request, res: Response): Promise<Response> {
    const customers = await Customer.findAll();

    return res.json(customers);
  }

  async show(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const customer = await Customer.findByPk(id, { include: ["Team"] });

    return customer ? res.json(customer) : res.status(404).json();
  }

  async store(req: Request, res: Response): Promise<Response> {
    const customer = await Customer.create(req.body);

    return res.json(customer);
  }

  async update(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const customer = await Customer.findByPk(id);

    if (customer) {
      await customer.update(req.body);

      return res.json(customer);
    }

    return res.status(404).json();
  }

  async destroy(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const customer = await Customer.findByPk(id);

    if (customer) {
      await customer.destroy();

      return res.status(204).json();
    }

    return res.status(404).json();
  }
}

export default new CustomerController();
