import { Request, Response } from "express";
import { Item } from "../models";

class ItemController {
  async index(req: Request, res: Response): Promise<Response> {
    const items = await Item.findAll();

    return res.json(items);
  }

  async show(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const item = await Item.findByPk(id, { include: ["Order", "Product"] });

    return item ? res.json(item) : res.status(404).json();
  }

  async store(req: Request, res: Response): Promise<Response> {
    const item = await Item.create(req.body);

    return res.json(item);
  }

  async update(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const item = await Item.findByPk(id);

    if (item) {
      await item.update(req.body);

      return res.json(item);
    }

    return res.status(404).json();
  }

  async destroy(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const item = await Item.findByPk(id);

    if (item) {
      await item.destroy();

      return res.status(204).json();
    }

    return res.status(404).json();
  }
}

export default new ItemController();
