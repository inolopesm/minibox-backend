import { Request, Response } from "express";
import { Order } from "../models";

class OrderController {
  async index(req: Request, res: Response): Promise<Response> {
    const orders = await Order.findAll();

    return res.json(orders);
  }

  async show(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const order = await Order.findByPk(id, { include: ["Customer", "Items"] });

    return order ? res.json(order) : res.status(404).json();
  }

  async store(req: Request, res: Response): Promise<Response> {
    if (!req.body.closed) req.body.closed = false;

    const order = await Order.create(req.body);

    return res.json(order);
  }

  async update(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const order = await Order.findByPk(id);

    if (order) {
      await order.update(req.body);

      return res.json(order);
    }

    return res.status(404).json();
  }

  async destroy(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const order = await Order.findByPk(id);

    if (order) {
      await order.destroy();

      return res.status(204).json();
    }

    return res.status(404).json();
  }
}

export default new OrderController();
