import { Request, Response } from "express";
import { Product } from "../models";

class ProductController {
  async index(req: Request, res: Response): Promise<Response> {
    const products = await Product.findAll();

    return res.json(products);
  }

  async show(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const product = await Product.findByPk(id);

    return product ? res.json(product) : res.status(404).json();
  }

  async store(req: Request, res: Response): Promise<Response> {
    const product = await Product.create(req.body);

    return res.json(product);
  }

  async update(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const product = await Product.findByPk(id);

    if (product) {
      await product.update(req.body);

      return res.json(product);
    }

    return res.status(404).json();
  }

  async destroy(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const product = await Product.findByPk(id);

    if (product) {
      await product.destroy();

      return res.status(204).json();
    }

    return res.status(404).json();
  }
}

export default new ProductController();
