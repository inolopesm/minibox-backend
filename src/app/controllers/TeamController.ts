import { Request, Response } from "express";
import { Team } from "../models";

class TeamController {
  async index(req: Request, res: Response): Promise<Response> {
    const teams = await Team.findAll();

    return res.json(teams);
  }

  async show(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const team = await Team.findByPk(id, { include: ["Customers"] });

    return team ? res.json(team) : res.status(404).json();
  }

  async store(req: Request, res: Response): Promise<Response> {
    const team = await Team.create(req.body);

    return res.json(team);
  }

  async update(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const team = await Team.findByPk(id);

    if (team) {
      await team.update(req.body);

      return res.json(team);
    }

    return res.status(404).json();
  }

  async destroy(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const team = await Team.findByPk(id);

    if (team) {
      await team.destroy();

      return res.status(204).json();
    }

    return res.status(404).json();
  }
}

export default new TeamController();
