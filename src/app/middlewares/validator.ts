import { Request, Response, NextFunction } from "express";
import { ObjectSchema } from "yup";

async function validator(
  req: Request,
  res: Response,
  next: NextFunction,
  schema: ObjectSchema
): Promise<void | Response> {
  try {
    await schema.validate(req.body, { abortEarly: false });

    return next();
  } catch (err) {
    return res.status(422).json(err);
  }
}

export default validator;
