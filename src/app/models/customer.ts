import { Model, Sequelize, DataTypes } from "sequelize";
import { Team } from "./team";

export class Customer extends Model {
  public id!: number;
  public name!: string;
  public phoneNumber!: string;
  public TeamId!: number;

  public readonly Team?: Team;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

export default (sequelize: Sequelize): typeof Customer => {
  Customer.init(
    {
      name: DataTypes.STRING,
      phoneNumber: DataTypes.STRING,
      TeamId: DataTypes.INTEGER,
    },
    { sequelize }
  );

  return Customer;
};
