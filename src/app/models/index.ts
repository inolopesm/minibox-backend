import env from "env-var";
import { Sequelize } from "sequelize";
import team from "./team";
import customer from "./customer";
import product from "./product";
import order from "./order";
import item from "./item";

const DATABASE_URL = env.get("DATABASE_URL").required().asUrlString();

const sequelize = new Sequelize(DATABASE_URL, { logging: false });

const Team = team(sequelize);
const Customer = customer(sequelize);
const Product = product(sequelize);
const Order = order(sequelize);
const Item = item(sequelize);

Team.hasMany(Customer);
Customer.belongsTo(Team);

Customer.hasMany(Order);
Order.belongsTo(Customer);

Order.hasMany(Item);
Item.belongsTo(Order);

Product.hasMany(Item);
Item.belongsTo(Product);

sequelize.sync();

export { Team, Customer, Product, Order, Item };
