import { Model, Sequelize, DataTypes } from "sequelize";
import { Order } from "./order";
import { Product } from "./product";

export class Item extends Model {
  public id!: number;
  public OrderId!: number;
  public ProductId!: number;
  public quantity!: number;

  public readonly Order?: Order;
  public readonly Product?: Product;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

export default (sequelize: Sequelize): typeof Item => {
  Item.init(
    {
      OrderId: DataTypes.INTEGER,
      ProductId: DataTypes.INTEGER,
      quantity: DataTypes.INTEGER,
    },
    { sequelize }
  );

  return Item;
};
