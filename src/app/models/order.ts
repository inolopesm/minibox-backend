import { Sequelize, Model, DataTypes } from "sequelize";
import { Customer } from "./customer";
import { Item } from "./item";

export class Order extends Model {
  public id!: number;
  public CustomerId!: number;
  public closed!: boolean;

  public readonly Customer?: Customer;
  public readonly Items?: Item[];

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

export default (sequelize: Sequelize): typeof Order => {
  Order.init(
    {
      CustomerId: DataTypes.INTEGER,
      closed: DataTypes.BOOLEAN,
    },
    { sequelize }
  );

  return Order;
};
