import { Model, Sequelize, DataTypes } from "sequelize";

export class Product extends Model {
  public id!: number;
  public name!: string;
  public price!: number;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

export default (sequelize: Sequelize): typeof Product => {
  Product.init(
    {
      name: DataTypes.STRING,
      price: DataTypes.DECIMAL,
    },
    { sequelize }
  );

  return Product;
};
