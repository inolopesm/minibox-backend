import { Model, Sequelize, DataTypes } from "sequelize";
import { Customer } from "./customer";

export class Team extends Model {
  public id!: number;
  public name!: string;

  public readonly Customers?: Customer[];

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

export default (sequelize: Sequelize): typeof Team => {
  Team.init(
    {
      name: DataTypes.STRING,
    },
    { sequelize }
  );

  return Team;
};
