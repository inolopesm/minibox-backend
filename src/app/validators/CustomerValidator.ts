import { Request, Response, NextFunction } from "express";
import { Customer, Team } from "../models";
import { object, string, number } from "yup";
import validator from "../middlewares/validator";

class CustomerValidator {
  async store(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void | Response> {
    const schema = object().shape({
      name: string().required().max(20).lowercase(),
      phoneNumber: string()
        .required()
        .matches(/^\d{9,11}$/)
        .test(
          "unique",
          "unique validation failed",
          async (phoneNumber: string) =>
            !phoneNumber ||
            !(await Customer.findOne({ where: { phoneNumber } }))
        ),
      TeamId: number()
        .required()
        .integer()
        .test(
          "check",
          "team doest exist",
          async (id: number) => !id || !!(await Team.findByPk(id))
        ),
    });

    return validator(req, res, next, schema);
  }

  async update(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void | Response> {
    const schema = object().shape({
      name: string().max(20).lowercase(),
      phoneNumber: string()
        .matches(/^\d{9,11}$/)
        .test(
          "unique",
          "unique validation failed",
          async (phoneNumber: string) =>
            !phoneNumber ||
            !(await Customer.findOne({ where: { phoneNumber } }))
        ),
      TeamId: number()
        .integer()
        .test(
          "check",
          "team doest exist",
          async (id: number) => !id || !!(await Team.findByPk(id))
        ),
    });

    return validator(req, res, next, schema);
  }
}

export default new CustomerValidator();
