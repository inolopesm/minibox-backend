import { Request, Response, NextFunction } from "express";
import { Product, Order } from "../models";
import { object, number } from "yup";
import validator from "../middlewares/validator";

class ItemValidator {
  async store(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void | Response> {
    const schema = object().shape({
      OrderId: number()
        .required()
        .integer()
        .test(
          "check",
          "order doest exist",
          async (id: number) => !id || !!(await Order.findByPk(id))
        ),
      ProductId: number()
        .required()
        .integer()
        .test(
          "check",
          "product doest exist",
          async (id: number) => !id || !!(await Product.findByPk(id))
        ),
      quantity: number().required().integer().min(0),
    });

    return validator(req, res, next, schema);
  }

  async update(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void | Response> {
    const schema = object().shape({
      OrderId: number()
        .integer()
        .test(
          "check",
          "order doest exist",
          async (id: number) => !id || !!(await Order.findByPk(id))
        ),
      ProductId: number()
        .integer()
        .test(
          "check",
          "product doest exist",
          async (id: number) => !id || !!(await Product.findByPk(id))
        ),
      quantity: number().required().integer().min(0),
    });

    return validator(req, res, next, schema);
  }
}

export default new ItemValidator();
