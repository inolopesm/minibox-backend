import { Request, Response, NextFunction } from "express";
import { Customer } from "../models";
import { object, number, boolean } from "yup";
import validator from "../middlewares/validator";

class OrderValidator {
  async store(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void | Response> {
    const schema = object().shape({
      CustomerId: number()
        .required()
        .integer()
        .test(
          "check",
          "customer doest exist",
          async (id: number) => !id || !!(await Customer.findByPk(id))
        ),
      closed: boolean(),
    });

    return validator(req, res, next, schema);
  }

  async update(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void | Response> {
    const schema = object().shape({
      CustomerId: number()
        .integer()
        .test(
          "check",
          "customer doest exist",
          async (id: number) => !id || !!(await Customer.findByPk(id))
        ),
      closed: boolean(),
    });

    return validator(req, res, next, schema);
  }
}

export default new OrderValidator();
