import { Request, Response, NextFunction } from "express";
import { Product } from "../models";
import { object, string, number } from "yup";
import validator from "../middlewares/validator";

class ProductValidator {
  async store(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void | Response> {
    const schema = object().shape({
      name: string()
        .required()
        .max(20)
        .lowercase()
        .test(
          "unique",
          "unique validation failed",
          async (name: string) =>
            !name || !(await Product.findOne({ where: { name } }))
        ),
      price: number().required().min(0).max(99.99),
    });

    return validator(req, res, next, schema);
  }

  async update(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void | Response> {
    const schema = object().shape({
      name: string()
        .max(20)
        .lowercase()
        .test(
          "unique",
          "unique validation failed",
          async (name: string) =>
            !name || !(await Product.findOne({ where: { name } }))
        ),
      price: number().min(0).max(99.99),
    });

    return validator(req, res, next, schema);
  }
}

export default new ProductValidator();
