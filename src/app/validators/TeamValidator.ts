import { Request, Response, NextFunction } from "express";
import { Team } from "../models";
import { object, string } from "yup";
import validator from "../middlewares/validator";

class TeamValidator {
  async store(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void | Response> {
    const schema = object().shape({
      name: string()
        .required()
        .max(20)
        .lowercase()
        .test(
          "unique",
          "unique validation failed",
          async (name: string) =>
            !name || !(await Team.findOne({ where: { name } }))
        ),
    });

    return validator(req, res, next, schema);
  }

  async update(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void | Response> {
    const schema = object().shape({
      name: string()
        .max(20)
        .lowercase()
        .test(
          "unique",
          "unique validation failed",
          async (name: string) =>
            !name || !(await Team.findOne({ where: { name } }))
        ),
    });

    return validator(req, res, next, schema);
  }
}

export default new TeamValidator();
