import dotenv from "dotenv-safe";
dotenv.config();

import env from "env-var";
import server from "./server";

const PORT = env.get("PORT").required().asPortNumber();

server.listen(PORT, () => console.log(`Server escutando a porta ${PORT}`));
