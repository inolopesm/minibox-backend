import { Router } from "express";
import CustomerController from "../app/controllers/CustomerController";
import CustomerValidator from "../app/validators/CustomerValidator";

const routes = Router();

routes.get("/", CustomerController.index);
routes.get("/:id", CustomerController.show);
routes.post("/", CustomerValidator.store, CustomerController.store);
routes.patch("/:id", CustomerValidator.update, CustomerController.update);
routes.delete("/:id", CustomerController.destroy);

export default routes;
