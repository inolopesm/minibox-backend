import { Router } from "express";
import team from "./team";
import customer from "./customer";
import product from "./product";
import order from "./order";
import item from "./item";

const routes = Router();

routes.use("/teams", team);
routes.use("/customers", customer);
routes.use("/products", product);
routes.use("/orders", order);
routes.use("/items", item);

export default routes;
