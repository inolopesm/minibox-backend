import { Router } from "express";
import ItemController from "../app/controllers/ItemController";
import ItemValidator from "../app/validators/ItemValidator";

const routes = Router();

routes.get("/", ItemController.index);
routes.get("/:id", ItemController.show);
routes.post("/", ItemValidator.store, ItemController.store);
routes.patch("/:id", ItemValidator.update, ItemController.update);
routes.delete("/:id", ItemController.destroy);

export default routes;
