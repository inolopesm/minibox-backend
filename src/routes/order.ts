import { Router } from "express";
import OrderController from "../app/controllers/OrderController";
import OrderValidator from "../app/validators/OrderValidator";

const routes = Router();

routes.get("/", OrderController.index);
routes.get("/:id", OrderController.show);
routes.post("/", OrderValidator.store, OrderController.store);
routes.patch("/:id", OrderValidator.update, OrderController.update);
routes.delete("/:id", OrderController.destroy);

export default routes;
