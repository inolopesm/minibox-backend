import { Router } from "express";
import ProductController from "../app/controllers/ProductController";
import ProductValidator from "../app/validators/ProductValidator";

const routes = Router();

routes.get("/", ProductController.index);
routes.get("/:id", ProductController.show);
routes.post("/", ProductValidator.store, ProductController.store);
routes.patch("/:id", ProductValidator.update, ProductController.update);
routes.delete("/:id", ProductController.destroy);

export default routes;
