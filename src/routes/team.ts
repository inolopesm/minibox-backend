import { Router } from "express";
import TeamController from "../app/controllers/TeamController";
import TeamValidator from "../app/validators/TeamValidator";

const routes = Router();

routes.get("/", TeamController.index);
routes.get("/:id", TeamController.show);
routes.post("/", TeamValidator.store, TeamController.store);
routes.patch("/:id", TeamValidator.update, TeamController.update);
routes.delete("/:id", TeamController.destroy);

export default routes;
