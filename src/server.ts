import express from "express";
import morgan from "morgan";
import routes from "./routes";

class Server {
  public express: express.Express;

  constructor() {
    this.express = express();

    this.middlewares();

    this.express.use(routes);
  }

  private middlewares(): void {
    this.express.use(express.json());
    this.express.use(morgan("dev"));
  }
}

export default new Server().express;
